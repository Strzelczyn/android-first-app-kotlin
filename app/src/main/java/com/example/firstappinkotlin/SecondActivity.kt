package com.example.firstappinkotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        passordFun(password, warn_password)
        emailFun(email, warn_email)
        /* radioGroup.setOnCheckedChangeListener { group, checkedId ->
             run {
                 var myIntent: Intent = Intent(applicationContext, MainActivity::class.java)
                 var RB: RadioButton = findViewById(checkedId)
                 myIntent.putExtra("sex", RB.text)
                 startActivity(myIntent)
             }
         }*/
    }

    fun submitData(v: View) {
        if (!checkBox.isChecked) {
            Toast.makeText(applicationContext, R.string.toast_terms, Toast.LENGTH_SHORT).show()

        } else {
            var myIntent: Intent = Intent(applicationContext, ProfileActivity::class.java)
            myIntent.putExtra("name", name.text.toString())
            myIntent.putExtra("password", password.text.toString())
            myIntent.putExtra("email", email.text.toString())
            myIntent.putExtra(
                "sex",
                findViewById<RadioButton>(radioGroup.checkedRadioButtonId).text
            )
            startActivity(myIntent)
        }

    }
}
