package com.example.firstappinkotlin

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonIntent.setOnClickListener {
            var newIntent = Intent(ACTION_VIEW, Uri.parse("https://www.google.com/"))
            startActivity(newIntent)
        }
        buttonActivity.setOnClickListener {
            var newActivity = Intent(applicationContext, SecondActivity::class.java)
            startActivity(newActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("sex")) {
            textsexmain.text = intent.getStringExtra("sex")
        }
    }
}
