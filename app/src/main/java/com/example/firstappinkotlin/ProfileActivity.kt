package com.example.firstappinkotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("name")) {
            nameProfile.text =
                resources.getString(R.string.profile_login) + " " + intent.getStringExtra("name")
        }
        if (intent.hasExtra("password")) {
            passwordProfile.text =
                resources.getString(R.string.profile_password) + " " + intent.getStringExtra("password")
        }
        if (intent.hasExtra("email")) {
            emailProfile.text =
                resources.getString(R.string.profile_email) + " " + intent.getStringExtra("email")
        }
        if (intent.hasExtra("sex")) {
            sexProfile.text =
                resources.getString(R.string.profile_sex) + " " + intent.getStringExtra("sex")
        }
    }
}
