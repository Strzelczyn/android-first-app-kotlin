package com.example.firstappinkotlin

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView

fun passordFun(password: EditText, warn_password: TextView) {
    password.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (password.length() == 0) {
                warn_password.visibility = TextView.INVISIBLE
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            warn_password.visibility = TextView.VISIBLE
            if (password.length() >= 8) {
                warn_password.visibility = TextView.INVISIBLE
            }
        }
    })
}

fun emailFun(email: EditText, warn_email: TextView) {
    email.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (email.length() == 0) {
                warn_email.visibility = TextView.INVISIBLE
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            var dots: Int = 0
            var at: Int = 0
            warn_email.visibility = TextView.VISIBLE
            email.text.toString().forEach { i ->
                when (i) {
                    '.' -> {
                        ++dots
                    }
                    '@' -> {
                        ++at
                    }
                }
            }
            if (dots == 1 && at == 1) {
                warn_email.visibility = TextView.INVISIBLE
            }
        }
    })
}
